public class Circle {
    Point center;
    int radius;

    /*public Circle(Point center, int radius) {
        this.center = center;
        this.radius = radius;
    }*/

    public double area() {
        return (Math.PI * radius * radius);
    }

    public double perimeter() {
        return (2 * Math.PI * radius);
    }

    public boolean intersect(Circle circle) {

        double x;
        x = center.xCoord - circle.center.xCoord;
        double y;
        y = center.yCoord - circle.center.yCoord;

        double distance;
        distance = Math.sqrt((x * x) + (y * y));

        if (distance < radius + circle.radius) return true;
        else return false;
    }

    /*public static void main(String[] args) {
        Point center = new Point(Integer.parseInt(args[0]),Integer.parseInt(args[1]));
        int radius = Integer.parseInt(args[3]);
        Circle circle = new Circle(center, radius);
        System.out.println(circle.area());
        System.out.println(circle.perimeter());
    }*/

}

