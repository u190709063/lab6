import java.util.Arrays;

public class Rectangle {
    int sideA,sideB;
    Point topLeft;

    public Rectangle( int sideA, int sideB, Point topLeft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topLeft = topLeft;
    }


    public int area() {
        return sideA * sideB;
    }

    public int perimeter() {
        return (sideA + sideB)*2;
    }

    public String corners(){
        return "| %d , %d | %d , %d | %s | %d , %d |".formatted(topLeft.xCoord, topLeft.yCoord, topLeft.xCoord + sideA, topLeft.yCoord, topLeft.xCoord + sideA + " , " + (topLeft.yCoord + sideB), topLeft.xCoord, topLeft.yCoord + sideB);
    }
   /* public static void main(String[] args) {
        Point corner = new Point(Integer.parseInt(args[0]),Integer.parseInt(args[1]));
        Point corner2 = new Point(Integer.parseInt(args[2]),Integer.parseInt(args[3]));
        Rectangle rectangle = new Rectangle(5,5,);
        System.out.println(rectangle.area());
        System.out.println(rectangle.perimeter());
    }*/

}
